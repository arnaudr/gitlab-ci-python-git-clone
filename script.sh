#!/bin/bash

set -eux

# dump stuff
git config -l | sort -u
git status
git branch

# creates a tmpdir under /tmp
tmpdir=$(mktemp -d)
# creates a tmpdir under /builds, ie. same mount point as the work dir,
# but it doesn't change anything to the issue
#tmpdir=$(mktemp -d --tmpdir=$(realpath ..) tmp.XXXXXX)

cd $tmpdir

# dump git config - no more expected at this point,
# as we're in tmpdir, not a git repo anymore...
git config -l

# run a python script
#cp $OLDPWD/script.py .
#./script.py $OLDPWD

# run stuff manually
#git clone $OLDPWD ./work/foobar
git clone -c init.defaultbranch=master $OLDPWD ./work/foobar
cd work/foobar
git branch
