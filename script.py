#!/usr/bin/python3

import os
import sys

import git

git_url = sys.argv[1]
checkoutdir = "./work/foobar"
branch = "master"

print(f"Here i am: {os.getcwd()}")
print(f"Cloning from {git_url} into {checkoutdir}")

repo = git.Repo.clone_from(git_url, checkoutdir)

print(f"Listing {checkoutdir}:")
print(f"{sorted(os.listdir(checkoutdir))}")
print(f"{sorted(os.listdir(checkoutdir + '/.git'))}")

all_branches = repo.git.branch()
print(f"branches: {all_branches}")

try:
    repo.git.checkout("remotes/origin/" + branch)
except Exception:
    repo.git.checkout(branch)
